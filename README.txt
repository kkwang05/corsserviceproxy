				
				CORS SERVICE PROXY

A simple JavaScript library for enabling JSON web service invocation by clients hosted on different (or external) server domains. 
An alternative to JSONP.

1. To allow cross-domain client access, the service needs to add the following two files in its virtual directory:
	
	Scripts/CorsServiceProxy.js
	CorsMessenger.html
 
2. To conduct cross-domain service calls, the client (e.g., SPA) needs only include the following in its index.html:

	<script type="text/javascript" src="https://partner.com/service/Scripts/CorsServiceProxy.js" />
	
Then (e.g.)

	CorsServiceProxy.CallService("api/foo", {arg0: "bar", arg1: true}, 
		function(response) {
			// Consume service call response
		});