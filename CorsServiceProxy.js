
var CorsServiceProxy = CorsServiceProxy || new function () {
	// Internal state
    var MSGR_URI = null;
    var iframe = null;
    var callbacks = {};
    
    $.each($('script'), function (i, e) {
    		// Convention based URI
            var idx = e.src.indexOf('Scripts/CorsServiceProxy.js');
            MSGR_URI = e.src.substring(0, idx) + 'CorsMessenger.html';
            return false;
    });

    $('<iframe />', {
        name: 'corsPrxy',
        id: 'corsPrxy',
        src: MSGR_URI
    }).appendTo('body').hide();

    $('#corsPrxy').load(function () {
        iframe = $('#corsPrxy').get(0).contentWindow;
    });

    function receiveMessage(event) {
        if (event.data.channel != 'fsvPrxy')
            return;
        var corrId = event.data.correlationId;
        var callbackFunc = callbacks[corrId];
        if (callbackFunc != null) {
            callbackFunc.call(null, rsp);
            delete callbacks[corrId];
        }
    };
    window.addEventListener('message', receiveMessage, false);
    
    // Public Interface
    this.CallService = function (op, pl, cb) {
        var corrId = Date.now();
        callbacks[corrId] = cb;        
        var payload = JSON.stringify(pl);
        var msg = { action: op, payload: payload, correlationId: corrId };
        iframe.postMessage(msg, MSGR_URI);
    }
};